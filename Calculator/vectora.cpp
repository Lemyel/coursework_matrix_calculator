#include "matrix.h"
#include "vectora.h"
#include "math.h"

class Matrix;
// перегрузка оператора присваивания
Vectora& Vectora::operator=(const Vectora& a)
{
    if (cols > 0)
    {
        for (int i = 0; i < rows; i++) // освободить память, выделенную ранее для объекта
            delete[] A[i];
    }
    if (rows > 0)
    {
        delete[] A;
    }

    rows = a.rows; // Копирование данных
    cols = a.cols;

    A = new double* [rows]; // Выделение памяти
    for (int i = 0; i < rows; i++)
        A[i] = new double[cols];

    for (int i = 0; i < rows; i++) // заполние значениями
        for (int j = 0; j < cols; j++)
            A[i][j] = a.A[i][j];
    return *this;
}
//перегрузка оператора +=
void Vectora::operator+=(Vectora const& a)
{
    for (int i = 0; i < rows; ++i)
        for (int j = 0; j < 1; ++j)
        {
            A[i][j] += a.A[i][j];
        }
}
//перегрузка оператора +
Vectora Vectora::operator+(Vectora const& a) const
{
    if (rows == a.rows)
    {
        Vectora A(*this);
        A += a;
        return A;
    }
    else exit(0);

}
//перегрузка оператора -=
void Vectora::operator-=(Vectora const& a)
{
    for (int i = 0; i < rows; ++i)
        for (int j = 0; j < cols; ++j)
        {
            A[i][j] -= a.A[i][j];
        }
}
//перегрузка оператора -
Vectora Vectora::operator-(Vectora const& a) const
{
    if (rows == a.rows)
    {
        Vectora A(*this);
        A -= a;
        return A;
    }
    else exit(0);
}
//перегрузка оператора *=
void operator*=(const Vectora& a, double right)
{
    for (int i = 0; i < a.rows; ++i)
        for (int j = 0; j < 1; ++j)
        {
            a.A[i][j] *= right;
        }
}
//перегрузка умножения матрицы на число
Vectora operator*(const Vectora& a, double right)
{
    Vectora A(a);
    A *= right;
    return A;

}
//перегрузка умножения числа на матрицу
Vectora operator*(double right, Vectora& a)
{
    Vectora A(a);
    A *= right;
    return A;
}
//длина вектора
double Vectora::Vector_length()
{
    double length=0;
    for (int i = 0; i < rows; i++)
    {
            length += pow(A[i][0], 2);
    }
    return sqrt(length);
}
//скалярное произведение
double Vectora::Scalar_product(Vectora& b)
{
    if (rows == b.rows)
    {
        double scalar=0;
        for(int i=0;i<rows;++i)
        scalar +=A[i][0]*b.A[i][0];
        return scalar;
    }
    else exit(0);
}
//векторное произведение
Vectora Vectora::Vector_product(const Vectora& b)
{
    if ((rows == 3) && (b.rows == 3))
    {
        Vectora c(rows);
        c.SetA(0, 0, A[1][0] * b.A[2][0]- A[2][0] * b.A[1][0]);
        c.SetA(1, 0, -A[0][0] * b.A[2][0] + A[2][0] * b.A[0][0]);
        c.SetA(2, 0, A[0][0] * b.A[1][0] - A[1][0] * b.A[0][0]);
        return c;
    }
    else exit(0);

}
Vectora Vectora::GuidesCos()
{
    Vectora V(rows);
    if (rows == 3)
    {
    for(int i=0;i<rows;i++)
        for(int j=0;j<cols;j++)
        V.SetA(i,j,A[i][j]/this->Vector_length());
    }
    return V;
}
