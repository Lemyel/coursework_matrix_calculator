#include "general.h"
#include "ui_general.h"
#include "qdynamicline.h"
#include "qdynamiclabel.h"
#include "matrix.h"
#include "vectora.h"
#include "math.h"
#include <QMouseEvent>
#include <QPainter>
#include <QStyleOption>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_okA_clicked()
{
    while (QLayoutItem* item = ui->gridLayout->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }

    zrow = ui->line_strokiA->text().toInt();
    zcol = ui->line_stolbciA->text().toInt();
    Matrix A(zrow,zcol); A1=A;
    QDynamicLine *lineA[zrow][zcol];
    for (int i =0;i<zrow ;i++ )
    {
       for (int j =0;j<zcol ;j++ )
       {
          lineA[i][j]=new QDynamicLine;// Создаем объект динамическ обьект LineEdit
          lineA[i][j]->setText(QString::number(0));
           lineA[i][j]->setStyleSheet("QDynamicLine{border: 1px solid black;font-size: 16px;color:black;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
           lineA[i][j]->setMaximumSize(80,20);
            ui->gridLayout->addWidget(lineA[i][j],i,j);
           lineA[i][j]->setObjectName(QString::number(i*zcol+j));
           connect(lineA[i][j], SIGNAL(textChanged(const QString&)), this, SLOT(slotGetNumberA() ));
        }
    }
}
void MainWindow::on_okB_clicked()
{
    while (QLayoutItem* item = ui->gridLayout_2->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    zrow1 = ui->line_strokiB->text().toInt();
    zcol1 = ui->line_stolbciB->text().toInt();
    Matrix B(zrow1,zcol1); B1=B; 
    QDynamicLine *lineB[zrow1][zcol1];
    for (int i =0;i<zrow1 ;i++ )
        for (int j =0;j<zcol1 ;j++ )
    {
        lineB[i][j]=new QDynamicLine;  // Создаем объект динамической кнопки
        lineB[i][j]->setText(QString::number(0));
        lineB[i][j]->setStyleSheet("QLineEdit{color:black;border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
        lineB[i][j]->setMaximumSize(80,20);
        ui->gridLayout_2->addWidget(lineB[i][j],i,j);
        lineB[i][j]->setObjectName(QString::number(i*zcol1+j));
        connect(lineB[i][j], SIGNAL(textChanged(const QString&)), this, SLOT(slotGetNumberB() ));
    }

}
void MainWindow::slotGetNumberA()
{
    int i,j;
    QDynamicLine* line=(QDynamicLine*) sender();
    int namebutton= (line->objectName()).toInt();
    double value;
    value = (line->text()).toDouble();
    if(namebutton%zcol==0)
    {
        i=(namebutton/zcol);
        j=0;
    }
    else
    { i=(namebutton/zcol);
       j=namebutton%zcol;
    }
    A1.SetA(i,j,value);
}
void MainWindow::slotGetNumberB()
{
    int i,j;
    QDynamicLine* line=(QDynamicLine*) sender();
    int namebutton= (line->objectName()).toInt();
    double value;
    value = (line->text()).toDouble();

    if(namebutton%zcol1==0)
    {
        i=(namebutton/zcol1);
         j=0;
    }
    else
    { i=(namebutton/zcol1);
       j=namebutton%zcol1;
    }
    B1.SetA(i,j,value);
}
void MainWindow::on_Summa_clicked()
{
    while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    if(zrow==zrow1 && zcol==zcol1)
        {
          Matrix S = A1+B1;
          QDynamicLabel *znach[zrow][zcol];// Создаем объект динамической кнопки
          for (int i =0;i<zrow ;i++ )
              for (int j =0;j<zcol ;j++ )
          {
              znach[i][j]=new QDynamicLabel;
              znach[i][j]->setText(QString::number(round((S.GetA(i,j))*10000)/10000,'g',8));
              znach[i][j]->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
              znach[i][j]->setMaximumSize(80,20);
              ui->gridLayout_4->addWidget(znach[i][j],i,j);
          }
        }
    else { QMessageBox::warning(this,"ОШИБКА!","Сложение матриц определено только для матриц одинаковой размерности!");}
}
void MainWindow::on_Raznost_clicked()
{
    while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    if(zrow==zrow1 && zcol==zcol1)
      {
          QDynamicLabel *znach[zrow][zcol];// Создаем объект динамической кнопки
          Matrix R =A1-B1;
          for (int i =0;i<zrow ;i++ )
              for (int j =0;j<zcol ;j++ )
          {
              znach[i][j]=new QDynamicLabel;
              znach[i][j]->setText(QString::number(round((R.GetA(i,j))*10000)/10000,'g',8));
              znach[i][j]->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
              znach[i][j]->setMaximumSize(80,20);
              ui->gridLayout_4->addWidget(znach[i][j],i,j);
          }
       }
    else { QMessageBox::warning(this,"ОШИБКА!","Разность матриц определена только для матриц одинаковой размерности!");}
}
void MainWindow::on_Umnoj_clicked()
{
    while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    if(zcol==zrow1)
      {   Matrix C;
          C=A1*B1;
          QDynamicLabel *znach[zrow][zcol1];// Создаем объект динамической кнопки
          for (int i =0;i<zrow ;i++ )
              for (int j =0;j<zcol1 ;j++ )
          {
              znach[i][j]=new QDynamicLabel;
              znach[i][j]->setText(QString::number(round((C.GetA(i,j))*100000)/100000,'g',8));
              znach[i][j]->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
              znach[i][j]->setMaximumSize(80,20);
              ui->gridLayout_4->addWidget(znach[i][j],i,j);
          }
       }
    else { QMessageBox::warning(this,"ОШИБКА!","Матрицы несогласованы!");}

}
void MainWindow::on_Stepen_clicked()
{
    while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    if(zrow==zcol)
      {
          QDynamicLabel *znach[zrow][zcol];// Создаем объект динамической кнопки
          for (int i =0;i<zrow ;i++ )
              for (int j =0;j<zcol ;j++ )
          {
              znach[i][j]=new QDynamicLabel;
              znach[i][j]->setText(QString::number(round((A1.Power( (ui->stepenline->text()).toInt() ).GetA(i,j))*100000)/100000,'g',8));
              znach[i][j]->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
              znach[i][j]->setMaximumSize(90,20);
              ui->gridLayout_4->addWidget(znach[i][j],i,j);
          }
       }
    else { QMessageBox::warning(this,"ОШИБКА!","В степень можно возвести только квадратную матрицу!");}
}
void MainWindow::on_Nachislo_clicked()
{
    while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    QDynamicLabel *znach[zrow][zcol];// Создаем объект динамической кнопки
    for (int i =0;i<zrow ;i++ )
        for (int j =0;j<zcol ;j++ )
    {
        znach[i][j]=new QDynamicLabel;
        znach[i][j]->setText(QString::number(round((((ui->nachisloline->text()).toDouble())*A1.GetA(i,j))*10000)/10000,'g',8));
        znach[i][j]->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
        znach[i][j]->setMaximumSize(80,20);
        ui->gridLayout_4->addWidget(znach[i][j],i,j);
}
}
void MainWindow::on_Transpose_clicked()
{
    while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    QDynamicLabel *znach[zcol][zrow];// Создаем объект динамической кнопки
    for (int i =0;i<zcol ;i++ )
        for (int j =0;j<zrow ;j++ )
    {
        znach[i][j]=new QDynamicLabel;
        znach[i][j]->setText(QString::number(round(((A1.Transpose()).GetA(i,j))*10000)/10000,'g',8));
        znach[i][j]->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
        znach[i][j]->setMaximumSize(80,20);
        ui->gridLayout_4->addWidget(znach[i][j],i,j);
}
}
void MainWindow::on_Det_clicked()
{ if(zrow==zcol)
    {
        while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
        {
            delete item->widget();
            delete item;
        }
        QDynamicLabel *det =new QDynamicLabel;// Создаем объект динамической кнопки
        det->setText("Определитель A: "+QString::number(round((A1.Determinant())*100000)/100000,'g',8));
        det->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
        det->setMaximumSize(250,40);
         ui->gridLayout_4->addWidget(det,0,0);

    }
  else { QMessageBox::warning(this,"ОШИБКА!","Определитель можно определить только для квадратных матриц!");}
}
void MainWindow::on_Slae_clicked()
{
    while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    if(zrow != zcol)
       {
           QMessageBox::warning(this,"ОШИБКА!","Первая матрица должна быть квадратной");
       }
       else {if(zcol1!=1)
       {
           QMessageBox::warning(this,"ОШИБКА!","Вторая матрица должна быть вектором");
       }
       else {if(zcol!=zrow1)
       {
           QMessageBox::warning(this,"ОШИБКА!","Количество неизвестных должно равняться числу свободных переменных");
       }
       else
       {
       QDynamicLabel *znach[zrow][zcol1];// Создаем объект динамической кнопки
       for (int i =0;i<zrow ;i++ )
           for (int j =0;j<zcol1 ;j++ )
       {
           znach[i][j]=new QDynamicLabel;
           znach[i][j]->setText(QString::number(round(((A1.Slu(B1)).GetA(i,j))*100000)/100000,'g',8));
           znach[i][j]->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
           znach[i][j]->setMaximumSize(90,20);
           ui->gridLayout_4->addWidget(znach[i][j],i,j);
       }
            }}}
}
void MainWindow::on_Inverse_clicked()
{
    if(zrow!=zcol){QMessageBox::warning(this,"ОШИБКА!","Обратную матрицу можно определить только для квадратной матрицы");}
    else
    {
        if(A1.Determinant()!=0)
        {
            while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
            {
                delete item->widget();
                delete item;
            }
            QDynamicLabel *znach[zcol][zrow];// Создаем объект динамической кнопки
            for (int i =0;i<zcol ;i++ )
                for (int j =0;j<zrow ;j++ )
            {
                znach[i][j]=new QDynamicLabel;
                znach[i][j]->setText(QString::number(round(((A1.Inverse()).GetA(i,j))*100000)/100000,'g',8));
                znach[i][j]->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
                znach[i][j]->setMaximumSize(90,20);
                ui->gridLayout_4->addWidget(znach[i][j],i,j);
            }
        }
        else { QMessageBox::warning(this,"ОШИБКА!","Обратная матрица существует только для невырожденной матрицы");}
    }
}
void MainWindow::on_VectorProduct_clicked()
{  while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    if((zcol!=1)||(zcol1!=1))
    {
         QMessageBox::warning(this,"ОШИБКА!","Это не вектора");
    }
    if((zrow!=3)||(zrow1!=3))
        {
             QMessageBox::warning(this,"ОШИБКА!","Некорректная длина векторов");
        }
    if((zrow==3)&&(zrow1==3)&&(zcol==1)&&(zcol1==1))
    {
        QDynamicLabel *znach[zrow][zcol];// Создаем объект динамической кнопки
        Vectora VA = A1;
        Vectora VB = B1;
        for (int i =0;i<zrow ;i++ )
            for (int j =0;j<zcol ;j++ )
        {
            znach[i][j]=new QDynamicLabel;
            znach[i][j]->setText(QString::number(round((VA.Vector_product(VB).GetA(i,j))*100000)/100000,'g',8));
            znach[i][j]->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
            znach[i][j]->setMaximumSize(90,20);
            ui->gridLayout_4->addWidget(znach[i][j],i,j);
        }
    }
}
void MainWindow::on_ScalarProduct_clicked()
{
    while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    if(zrow==zrow1)
    {
        Vectora VA = A1;
        Vectora VB = B1;
        QDynamicLabel *scalar =new QDynamicLabel;// Создаем объект динамической кнопки
        scalar->setText("Скалярное произведение: "+QString::number(round((VA.Scalar_product(VB))*100000)/100000,'g',8));
        scalar->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
        scalar->setMaximumSize(250,40);
         ui->gridLayout_4->addWidget(scalar,0,0);

    }
    else
    {
      if((zcol!=1)||(zcol1!=1)) QMessageBox::warning(this,"ОШИБКА!","Это не вектора");
      else QMessageBox::warning(this,"ОШИБКА!","Вектора должны быть одинаковой длины");
    }
}
void MainWindow::on_VectorLength_clicked()
{
    while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    if((zcol!=1))
    {
         QMessageBox::warning(this,"ОШИБКА!","Это не вектор");
    }
    else{
        Vectora VA=A1;
        QDynamicLabel *length =new QDynamicLabel;// Создаем объект динамической кнопки
        length->setText("Длина вектора: "+QString::number(round((VA.Vector_length())*100000)/100000,'g',8));
        length->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
        length->setMaximumSize(250,40);
        ui->gridLayout_4->addWidget(length,0,0);
    }
}
void MainWindow::on_GuidesCos_clicked()
{
    while (QLayoutItem* item = ui->gridLayout_4->takeAt(0)) //удаление предыдущих виджетов
    {
        delete item->widget();
        delete item;
    }
    if(zcol!=1) QMessageBox::warning(this,"ОШИБКА!","Это не вектора");
    else
    { if(zrow!=3) QMessageBox::warning(this,"ОШИБКА!","Некорректная длина вектора");
        else
        {
            QDynamicLabel *znach[zrow][zcol+1];// Создаем объект динамической кнопки
            Vectora VA = A1;
            for (int i =0;i<zrow ;i++ )
                for (int j =0;j<zcol ;j++ )
            {   znach[i][j]=new QDynamicLabel;
                znach[i][j]->setText("cos("+QString::number(i+1)+") = "+QString::number(round((VA.GuidesCos()).GetA(i,j)*100000)/100000,'g',8));
                znach[i][j]->setStyleSheet("QLabel{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}");
                znach[i][j]->setMaximumSize(200,20);
                ui->gridLayout_4->addWidget(znach[i][j],i,j);
            }
        }

    }
}
// две функции снизу для перемещения окна
void MainWindow::mousePressEvent(QMouseEvent *event)
{
startPos = event->pos();
QWidget::mousePressEvent(event);
}
void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
QPoint delta = event->pos() - startPos;
QWidget * w = window();
if(w)
w->move(w->pos() + delta);
QWidget::mouseMoveEvent(event);
}
//закрытие окна
void MainWindow::on_X_clicked()
{
    this->close();
}
//сворачивание окна
void MainWindow::on_turn_clicked()
{
    this->showMinimized();
}

void MainWindow::on_RU_clicked()
{
    ui->GuidesCos->setText("Направляющие cos А");
    ui->VectorLength->setText("Длина вектора А");
    ui->ScalarProduct->setText("Скалярное произведение");
    ui->VectorProduct->setText("Векторное произведение");
    ui->Inverse->setText("Обратная матрица А");
    ui->Slae->setText("СЛАУ");
    ui->Det->setText("Определтель А");
    ui->Nachislo->setText("Умножить на число A");
    ui->Transpose->setText("Транспонировать А");
}
void MainWindow::on_EN_clicked()
{
    ui->GuidesCos->setText("Guides cos() A");
    ui->VectorLength->setText("Vector length A");
    ui->ScalarProduct->setText("Scalar product");
    ui->VectorProduct->setText("Vector product");
    ui->Inverse->setText("Inverse А");
    ui->Slae->setText("SLAE");
    ui->Det->setText("Determinant А");
    ui->Nachislo->setText("Multiply by number A");
    ui->Transpose->setText("Transpose А");
}

