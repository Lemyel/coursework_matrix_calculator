#ifndef QDYNAMICBUTTON_H
#define QDYNAMICBUTTON_H
#include <QLineEdit>

class QDynamicLine : public QLineEdit
{
    Q_OBJECT
public:
    explicit QDynamicLine(QWidget *parent = 0);
    ~QDynamicLine();
    static int ResID;
    int getlineID();

public slots:

private:
    int lineID = 0;
};

#endif // QDYNAMICBUTTON_H
