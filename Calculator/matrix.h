#pragma once

#ifndef MATRIX_H
#define MATRIX_H
#include <iostream>
#include <string>
using namespace std;

class Matrix
{
protected:
    double** A;
    double** C=0;
    double det=0;
    int rows; // строки
    int cols; // столбцы
public:
    // Конструктор по-умолчанию
    Matrix()
    {
        A = 0;
        cols = 0;
        rows = 0;
    }
    // Конструктор с параметрами
    Matrix(int Rows, int Cols)
    {
        if ((Rows < 0)  || (Cols < 0) )
       {
          exit(0);
        }
        else
       {
            rows = Rows; cols = Cols;
            A = new double* [rows];  // Выделение памяти
            for (int i = 0; i < rows; i++)
                A[i] = new double[cols];

        for (int i = 0; i < rows; i++) // нулевая матрица
            for (int j = 0; j < cols; j++)
                A[i][j] = 0;
       }
    }
    // Конструктор копирования
    Matrix(const Matrix& a)
    {
        rows = a.rows; // Создается новые обьекты
        cols = a.cols;

        A =  new double * [rows]; // Выделение памяти
        for (int i = 0; i < rows; i++)
            A[i] = new double[cols];

        for (int i = 0; i < rows; i++)// заполние элементов новой матрицы, значениями старой матрицы
            for (int j = 0; j < cols; j++)
                A[i][j] = a.A[i][j];
    }
    // Деструктор, освобождает память
    ~Matrix()
    {
        for (int i = 0; i < rows; i++)  delete[] A[i];
        delete[] A;
    }
    // метод для установки значений элементов
    void SetA(int i, int j, double value);
    // метод для получения значения определенного элемента
    double GetA(int i, int j);
    // метод ВЫВОДА матрицы на экран
    void Print(const string MatrixName);
    // метод для получения значения количества строк
    double GetRows();
    // метод для получения значения количества столбцов
    double GetCols();
    // перегрузка оператора присваивания
    Matrix& operator=(const Matrix& a);
    //перегрузка оператора +=
    void operator+=(Matrix const& a);
    //перегрузка оператора +
    Matrix operator+(Matrix const& a) const;
    //перегрузка(по-другому) оператора -
     /*  Matrix operator-(Matrix const& a) const;*/
    //перегрузка оператора -=
    void operator-=(Matrix const& a);
    //перегрузка оператора -
    Matrix operator-(Matrix const& a) const;
    //перегрузка оператора *
    Matrix operator*(Matrix const& a);
    //перегрузка оператора *=
    friend void operator*=(const Matrix& a, double right);
    //перегрузка умножения матрицы на число
    friend Matrix operator*(const Matrix& a, double right);
    //перегрузка умножения числа на матрицу
     friend Matrix operator*(double right, Matrix& a);
    //Минор матрицы для элемента а[i][j]
    Matrix Minor(int row, int col);
    //Определитель Матрицы
    double Determinant();
    // Транспонирование матрицы
    Matrix Transpose();
    //Обратная матрица
    Matrix Inverse();
    //Возведение в степень
    Matrix Power(double n);
    //Решение СЛУ
    Matrix Slu(Matrix& a);
};

#endif // MATRIX_H
