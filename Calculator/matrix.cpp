#include "matrix.h"
#include "math.h"

// метод для установки значений элементов
void Matrix::SetA(int i, int j, double value)
{
    if ((i < 0) || (i >= rows))//проверка на корректность строк для разработчика
        exit(0);
    if ((j < 0) || (j >= cols))//проверка на корректность столбцов для разработчика
        exit(0);
    A[i][j] = value;
}
// метод для получения значения
double Matrix::GetA(int i, int j)
{
    if ((i >= 0) && (j >= 0)&& (i < rows) && (j < cols))//проверка на корректность строк и столбцов для разработчика
        return A[i][j];
    else
        return 101001;
}
// метод ВЫВОДА матрицы на экран
void Matrix::Print(const string MatrixName)
{
    cout << "Matrix: " << MatrixName << endl;
    for (int i = 0; i < (cols * 7); i++) cout << "-";
    cout << endl;
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
            cout << A[i][j] << "\t";
        cout << endl;
    }
    for (int i = 0; i < (cols * 7); i++) cout << "-";
    cout << endl << endl;
}
// метод для получения значения количества строк
double Matrix::GetRows()
{
    return rows;
}
// метод для получения значения количества столбцов
double Matrix::GetCols()
{
    return cols;
}
// перегрузка оператора присваивания
Matrix& Matrix::operator=(const Matrix& a)
{
    if (cols > 0)
    {
        for (int i = 0; i < rows; i++) // освободить память, выделенную ранее для объекта
            delete[] A[i];
    }
    if (rows > 0)
    {
        delete[] A;
    }

    rows = a.rows; // Копирование данных
    cols = a.cols;

    A = new double* [rows]; // Выделение памяти
    for (int i = 0; i < rows; i++)
        A[i] = new double[cols];

    for (int i = 0; i < rows; i++) // заполние значениями
        for (int j = 0; j < cols; j++)
            A[i][j] = a.A[i][j];
    return *this;
}
//перегрузка оператора +=
void Matrix::operator+=(Matrix const& a)
{
    for (int i = 0; i < rows; ++i)
        for (int j = 0; j < cols; ++j)
        {
            A[i][j] += a.A[i][j];
        }
}
//перегрузка оператора +
Matrix Matrix::operator+(Matrix const& a) const
{
    if ((rows == a.rows) && (cols == a.cols))
    {
        Matrix A(*this);
        A += a;
        return A;
    }
    else
    {
        exit(0);
    };
}
//перегрузка оператора -=
void Matrix::operator-=(Matrix const& a)
{
    for (int i = 0; i < rows; ++i)
        for (int j = 0; j < cols; ++j)
        {
            A[i][j] -= a.A[i][j];
        }
}
//перегрузка оператора -
Matrix Matrix::operator-(Matrix const& a) const
{
    if ((rows == a.rows) && (cols == a.cols))
    {
        Matrix A(*this);
        A -= a;
        return A;
    }
    else
    {
        exit(0);
    };
}
//перегрузка оператора *
Matrix Matrix::operator*(Matrix const& a)
{
    if (cols == a.rows) {
        Matrix V(rows, a.cols);
        C = new double* [rows]; // Выделение памяти
        for (int i = 0; i < rows; i++)
            C[i] = new double[a.cols];
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < a.cols; j++)
            {
                C[i][j] = 0;
                for (int k = 0; k < a.rows; k++)
                {
                    C[i][j] += (A[i][k] * a.A[k][j]);
                    V.SetA(i, j, C[i][j]);
                }
            }
        }
        for (int i = 0; i < rows; i++) // освободить память, выделенную ранее для объекта
            delete[] C[i];
        delete[] C;
        return V;
    }
    else
    {
        exit(0);
    }
}
//перегрузка оператора *=
void operator*=(const Matrix& a, double right)
{
    for (int i = 0; i < a.rows; ++i)
        for (int j = 0; j < a.cols; ++j)
        {
            a.A[i][j] *= right;
        }
}
//перегрузка умножения матрицы на число
Matrix operator*(const Matrix& a, double right)
{
    Matrix A(a);
    A *= right;
    return A;

}
//перегрузка умножения числа на матрицу
Matrix operator*(double right, Matrix& a)
{
    Matrix A(a);
    A *= right;
    return A;
}
//Минор матрицы для элемента а[i][j]
Matrix Matrix::Minor(int row, int col)
{
    if (rows != cols)
    {
        exit(0);
    }
    if ((row < 0) || ((int)row != row) || (col < 0) || ((int)col != col) || (row > rows) || (col > cols))
    {
        exit(0);
    }
    else
    {
        int r = rows - 1;
        Matrix L(r, r);
        int a = 0, b = 0; int proverka = 0;
        C = new double* [rows - 1]; // Выделение памяти
        for (int i = 0; i < rows - 1; i++)
        {
            C[i] = new double[rows - 1];
        }
        //записывание минора в новую матрицу
        for (int i = 0; i < rows; i++)
        {
            b = 0;
            for (int j = 0; j < rows; j++)
            {
                if (!((j == row) || (i == col))) // проверка на то, чтобы не записывались зачеркнутые строки и стобцы
                {
                    C[b][a] = A[j][i];
                    L.SetA(b, a, C[b][a]);
                    b++; // в строку "а" и столбец b =1...rows записываем значения из старой матрицы
                    proverka++;
                }
            }
            if (proverka != 0)
                a++;
            proverka = 0;
        }
        for (int i = 0; i < rows - 1; i++) // освободить память, выделенную ранее для объекта
            delete[] C[i];
        delete[] C;
        return L;
    }
}
//Определитель Матрицы
double Matrix::Determinant()
{
    det =0;
    if (rows == cols)
    {
        if (rows == 1)
        {
            det = A[0][0];
        }
        else
        {
            if (rows == 2)
            {
                det = A[0][0] * A[1][1] - A[0][1] * A[1][0];
            }
            else
            {
                for (int i = 0; i < rows; i++)
                {
                    det += (pow(-1, i) * A[0][i] * Minor(0, i).Determinant());
                }
            }
        }
        return det;
    }
    else
    {
        exit(0);
    }
}
// Транспонирование матрицы
Matrix Matrix::Transpose()
{
    Matrix T(cols, rows);
    C = new double* [cols]; // Выделение памяти
    for (int i = 0; i < cols; i++)
        C[i] = new double[rows];

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            C[j][i] = A[i][j];
            T.SetA(j, i, C[j][i]);
        }
        cout << endl;
    }
    for (int i = 0; i < cols; i++) // освободить память, выделенную ранее для объекта
        delete[] C[i];
    delete[] C;
    return T;
}
// Обратная матрица
Matrix Matrix::Inverse()
{
    Matrix I(*this);
    long double p = (1 / I.Determinant());
    double k = 0;
    if (rows != cols)
    {
        exit(0);
    }
    if (I.Determinant() == 0)
    {
        exit(0);
    }
    if (rows != 1)
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                k = pow(-1, i + j) * Minor(j, i).Determinant() * p;
                I.SetA(i, j, k);
            }
        }
         return I;
    }
    else
    {
        I.SetA(0, 0, 1 / I.GetA(0, 0));
        return I;
    }
}
//Возведение в степень
Matrix Matrix::Power(double n)
{
    if (rows == cols)
    {
        Matrix P(*this);
        Matrix A(*this);
        if (n > 1)
        {
            for (int i = 1; i < n; i++) {
                P = P * A;
            }
        }
        else
        {
            if (n == 1)
            {
                P = A;
            }
            else
            {
                if (n == 0)
                {
                    for (int i = 0; i < rows; i++)
                    {
                        for (int j = 0; j < rows; j++)
                        {
                            if (i == j)
                            {
                                P.SetA(i, j, 1);
                            }
                            else P.SetA(i, j, 0);
                        }
                    }
                }
                else
                {
                    if (n == -1)
                    {
                        P = A.Inverse();
                    }
                    else
                    {
                        if (n < -1)
                        {
                            for (int i = 1; i < (-n); i++)
                            {
                                P = P * A;
                            }
                            P = P.Inverse();
                        }
                    }
                }
            }
        }
        return P;
    }
    else
    {
        exit(0);
    }
}
//Решение СЛАУ
Matrix Matrix::Slu(Matrix& a)
{
    if (rows != cols)
    {
        exit(0);
    }
    if (a.cols != 1)
    {
        exit(0);
    }
    if (cols != a.rows)
    {
        exit(0);
    }
    return this->Inverse() * a;
}
