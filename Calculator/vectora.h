#pragma once
#include "matrix.h"

class Vectora : public Matrix
{
public:
    Vectora() :Matrix()
    { }
    Vectora(int Rows):Matrix(Rows,1)
    {  }
    // Конструктор копирования
    Vectora(const Vectora& a):Matrix(a)
    { }
    // Конструктор для копирования матрицы со строкой 1 в вектор
      Vectora(const Matrix& a):Matrix(a)
      {
        if (this->cols != 1) exit(0);
      }
    ~Vectora()
    { }
    //перегрузка оператора =
    Vectora& operator=(const Vectora& a);
    //перегрузка оператора +=
    void operator+=(Vectora const& a);
    //перегрузка оператора +
    Vectora operator+(Vectora const& a) const;
    //перегрузка(по-другому) оператора -
     /*  Matrix operator-(Matrix const& a) const;*/
    //перегрузка оператора -=
    void operator-=(Vectora const& a);
    //перегрузка оператора -
    Vectora operator-(Vectora const& a) const;
    //перегрузка оператора *=
    friend void operator*=(const Vectora& a, double right);
    //перегрузка умножения матрицы на число
    friend Vectora operator*(const Vectora& a, double right);
    //перегрузка умножения числа на матрицу
    friend Vectora operator*(double right, Vectora& a);
    //длина вектора
    double Vector_length();
    //векторное произведение
    Vectora Vector_product(const Vectora&b);
    //скалярное произведение
    double Scalar_product(Vectora& b);
    Vectora GuidesCos();
};
