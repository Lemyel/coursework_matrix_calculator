#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "matrix.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    int zrow1=0;
    int zcol1=0;
    int zrow=0;
    int zcol=0;
    Matrix A1;
    Matrix B1;
    Ui::MainWindow *ui;
    QPoint startPos;

private slots:
    void on_okA_clicked();
    void on_okB_clicked();
    void on_Summa_clicked();
    void slotGetNumberA();
    void slotGetNumberB();
    void on_Raznost_clicked();
    void on_Umnoj_clicked();
    void on_Stepen_clicked();
    void on_Nachislo_clicked();
    void on_Transpose_clicked();
    void on_Det_clicked();
    void on_Slae_clicked();
    void on_Inverse_clicked();
    void on_VectorProduct_clicked();
    void on_ScalarProduct_clicked();
    void on_VectorLength_clicked();
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void on_X_clicked();
    void on_turn_clicked();
    void on_GuidesCos_clicked();
    void on_RU_clicked();
    void on_EN_clicked();
};
#endif // MAINWINDOW_H
