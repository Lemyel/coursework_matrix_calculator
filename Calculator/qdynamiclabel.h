#ifndef QDYNAMICLABEL_H
#define QDYNAMICLABEL_H

#include <QLabel>

class QDynamicLabel : public QLabel
{
    Q_OBJECT
public:
    explicit QDynamicLabel(QWidget *parent = 0);
    ~QDynamicLabel();
    static int ResID;
    int getlabelID();
public slots:
private:
    int labelID = 0;
};

#endif // QDYNAMICLABEL_H

