/********************************************************************************
** Form generated from reading UI file 'general.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GENERAL_H
#define UI_GENERAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QLabel *strokiA;
    QLabel *stolbciA;
    QLabel *strokiB;
    QLabel *stolbciB;
    QLabel *Rez;
    QLabel *A;
    QLabel *B;
    QLabel *liniya1;
    QLabel *liniya2;
    QLabel *liniya3;
    QLabel *liniya4;
    QGroupBox *znachA;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QGroupBox *znachB;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout_2;
    QLineEdit *line_strokiA;
    QLineEdit *line_stolbciA;
    QLineEdit *line_strokiB;
    QLineEdit *line_stolbciB;
    QPushButton *okA;
    QPushButton *okB;
    QGroupBox *znachC;
    QWidget *layoutWidget_2;
    QGridLayout *gridLayout_4;
    QLineEdit *stepenline;
    QLineEdit *nachisloline;
    QPushButton *Summa;
    QPushButton *Nachislo;
    QPushButton *Transpose;
    QPushButton *Slae;
    QPushButton *Raznost;
    QPushButton *Stepen;
    QPushButton *Umnoj;
    QPushButton *Det;
    QPushButton *Inverse;
    QPushButton *VectorProduct;
    QPushButton *VectorLength;
    QPushButton *ScalarProduct;
    QPushButton *X;
    QPushButton *turn;
    QPushButton *GuidesCos;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 436);
        MainWindow->setMinimumSize(QSize(800, 436));
        MainWindow->setMaximumSize(QSize(800, 436));
        MainWindow->setStyleSheet(QString::fromUtf8("MainWindow{\n"
"  \n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"\n"
"}"));
        MainWindow->setUnifiedTitleAndToolBarOnMac(false);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        strokiA = new QLabel(centralwidget);
        strokiA->setObjectName(QString::fromUtf8("strokiA"));
        strokiA->setGeometry(QRect(200, 30, 64, 22));
        QFont font;
        font.setPointSize(11);
        font.setBold(true);
        font.setWeight(75);
        strokiA->setFont(font);
        strokiA->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color: rgb(124,145,238);\n"
"}"));
        stolbciA = new QLabel(centralwidget);
        stolbciA->setObjectName(QString::fromUtf8("stolbciA"));
        stolbciA->setGeometry(QRect(310, 30, 79, 22));
        stolbciA->setFont(font);
        stolbciA->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color:rgb(124,145,238);\n"
"}"));
        strokiB = new QLabel(centralwidget);
        strokiB->setObjectName(QString::fromUtf8("strokiB"));
        strokiB->setGeometry(QRect(500, 30, 64, 22));
        strokiB->setFont(font);
        strokiB->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color: rgb(124,145,238);\n"
"}"));
        stolbciB = new QLabel(centralwidget);
        stolbciB->setObjectName(QString::fromUtf8("stolbciB"));
        stolbciB->setGeometry(QRect(610, 30, 79, 22));
        stolbciB->setFont(font);
        stolbciB->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color: rgb(124,145,238);\n"
"}"));
        Rez = new QLabel(centralwidget);
        Rez->setObjectName(QString::fromUtf8("Rez"));
        Rez->setGeometry(QRect(240, 250, 86, 22));
        Rez->setFont(font);
        Rez->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color: rgb(75,120,255);\n"
"}"));
        A = new QLabel(centralwidget);
        A->setObjectName(QString::fromUtf8("A"));
        A->setGeometry(QRect(310, 6, 41, 20));
        QFont font1;
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setItalic(false);
        font1.setUnderline(true);
        font1.setWeight(75);
        A->setFont(font1);
        A->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:white;\n"
" background-color: rgb(124,145,238);\n"
"border-radius: 5px;\n"
"}\n"
""));
        B = new QLabel(centralwidget);
        B->setObjectName(QString::fromUtf8("B"));
        B->setGeometry(QRect(610, 6, 41, 20));
        B->setFont(font1);
        B->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:white;\n"
" background-color: rgb(124,145,238);\n"
"border-radius: 5px;\n"
"}"));
        liniya1 = new QLabel(centralwidget);
        liniya1->setObjectName(QString::fromUtf8("liniya1"));
        liniya1->setGeometry(QRect(180, 60, 623, 2));
        liniya1->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:#2442a2;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        liniya2 = new QLabel(centralwidget);
        liniya2->setObjectName(QString::fromUtf8("liniya2"));
        liniya2->setGeometry(QRect(180, 250, 622, 2));
        liniya2->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:#2442a2;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        liniya3 = new QLabel(centralwidget);
        liniya3->setObjectName(QString::fromUtf8("liniya3"));
        liniya3->setGeometry(QRect(490, 60, 1, 192));
        liniya3->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:#2442a2;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        liniya4 = new QLabel(centralwidget);
        liniya4->setObjectName(QString::fromUtf8("liniya4"));
        liniya4->setGeometry(QRect(179, -9, 2, 446));
        liniya4->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:#2442a2;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        znachA = new QGroupBox(centralwidget);
        znachA->setObjectName(QString::fromUtf8("znachA"));
        znachA->setGeometry(QRect(199, 62, 281, 188));
        znachA->setStyleSheet(QString::fromUtf8("QGroupBox{\n"
"  border: 0px;\n"
"  color:white;\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(124,145,238), stop: 1 rgb(81,119,243));\n"
"}"));
        layoutWidget = new QWidget(znachA);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 281, 191));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        znachB = new QGroupBox(centralwidget);
        znachB->setObjectName(QString::fromUtf8("znachB"));
        znachB->setGeometry(QRect(500, 62, 281, 188));
        znachB->setStyleSheet(QString::fromUtf8("QGroupBox{\n"
"  border: 0px;\n"
"  color:white;\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(124,145,238), stop: 1 rgb(81,119,243));\n"
"\n"
"}"));
        gridLayoutWidget = new QWidget(znachB);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(-1, -1, 281, 191));
        gridLayout_2 = new QGridLayout(gridLayoutWidget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        line_strokiA = new QLineEdit(centralwidget);
        line_strokiA->setObjectName(QString::fromUtf8("line_strokiA"));
        line_strokiA->setGeometry(QRect(270, 30, 31, 22));
        line_strokiA->setStyleSheet(QString::fromUtf8("QLineEdit\n"
"{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}"));
        line_stolbciA = new QLineEdit(centralwidget);
        line_stolbciA->setObjectName(QString::fromUtf8("line_stolbciA"));
        line_stolbciA->setGeometry(QRect(395, 30, 31, 22));
        line_stolbciA->setStyleSheet(QString::fromUtf8("QLineEdit\n"
"{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}"));
        line_strokiB = new QLineEdit(centralwidget);
        line_strokiB->setObjectName(QString::fromUtf8("line_strokiB"));
        line_strokiB->setGeometry(QRect(570, 30, 31, 22));
        line_strokiB->setStyleSheet(QString::fromUtf8("QLineEdit\n"
"{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}"));
        line_stolbciB = new QLineEdit(centralwidget);
        line_stolbciB->setObjectName(QString::fromUtf8("line_stolbciB"));
        line_stolbciB->setGeometry(QRect(694, 30, 31, 22));
        line_stolbciB->setStyleSheet(QString::fromUtf8("QLineEdit\n"
"{ background-color: white;color:black; border: 1px solid black;font-size: 16px;qproperty-alignment: 'AlignVCenter | AlignCenter';}"));
        okA = new QPushButton(centralwidget);
        okA->setObjectName(QString::fromUtf8("okA"));
        okA->setGeometry(QRect(440, 30, 40, 22));
        okA->setStyleSheet(QString::fromUtf8("QPushButton\n"
"{\n"
"background-color:#a9c7ff;\n"
"border:1px solid white;\n"
"border-radius: 6px;\n"
"\n"
"}"));
        okB = new QPushButton(centralwidget);
        okB->setObjectName(QString::fromUtf8("okB"));
        okB->setGeometry(QRect(740, 30, 40, 22));
        okB->setStyleSheet(QString::fromUtf8("QPushButton\n"
"{\n"
"background-color:#a9c7ff;\n"
"border:1px solid white;\n"
"border-radius: 6px;\n"
"\n"
"}"));
        znachC = new QGroupBox(centralwidget);
        znachC->setObjectName(QString::fromUtf8("znachC"));
        znachC->setGeometry(QRect(340, 253, 281, 183));
        znachC->setStyleSheet(QString::fromUtf8("QGroupBox{\n"
"  border: 0px;\n"
"  color:white;\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(75,123,255), stop: 1 rgb(54,101,243));\n"
"}"));
        layoutWidget_2 = new QWidget(znachC);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(0, 0, 281, 181));
        gridLayout_4 = new QGridLayout(layoutWidget_2);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        stepenline = new QLineEdit(centralwidget);
        stepenline->setObjectName(QString::fromUtf8("stepenline"));
        stepenline->setGeometry(QRect(148, 129, 31, 18));
        stepenline->setStyleSheet(QString::fromUtf8("QLineEdit\n"
"{ background-color: white;color:black; border: 1px solid black;font-size: 12px;qproperty-alignment: 'AlignVCenter | AlignCenter';}"));
        nachisloline = new QLineEdit(centralwidget);
        nachisloline->setObjectName(QString::fromUtf8("nachisloline"));
        nachisloline->setGeometry(QRect(148, 155, 31, 16));
        nachisloline->setStyleSheet(QString::fromUtf8("QLineEdit\n"
"{ background-color: white;color:black; border: 1px solid black;font-size: 12px;qproperty-alignment: 'AlignVCenter | AlignCenter';}"));
        Summa = new QPushButton(centralwidget);
        Summa->setObjectName(QString::fromUtf8("Summa"));
        Summa->setGeometry(QRect(1, 51, 178, 18));
        Summa->setFont(font);
        Summa->setAutoFillBackground(false);
        Summa->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        Nachislo = new QPushButton(centralwidget);
        Nachislo->setObjectName(QString::fromUtf8("Nachislo"));
        Nachislo->setGeometry(QRect(1, 155, 148, 16));
        QFont font2;
        font2.setPointSize(10);
        font2.setBold(true);
        font2.setWeight(75);
        Nachislo->setFont(font2);
        Nachislo->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        Transpose = new QPushButton(centralwidget);
        Transpose->setObjectName(QString::fromUtf8("Transpose"));
        Transpose->setGeometry(QRect(1, 179, 178, 16));
        Transpose->setFont(font2);
        Transpose->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        Slae = new QPushButton(centralwidget);
        Slae->setObjectName(QString::fromUtf8("Slae"));
        Slae->setGeometry(QRect(1, 227, 178, 16));
        Slae->setFont(font2);
        Slae->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        Raznost = new QPushButton(centralwidget);
        Raznost->setObjectName(QString::fromUtf8("Raznost"));
        Raznost->setGeometry(QRect(1, 77, 178, 18));
        Raznost->setFont(font);
        Raznost->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        Stepen = new QPushButton(centralwidget);
        Stepen->setObjectName(QString::fromUtf8("Stepen"));
        Stepen->setGeometry(QRect(1, 129, 148, 18));
        Stepen->setFont(font);
        Stepen->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        Umnoj = new QPushButton(centralwidget);
        Umnoj->setObjectName(QString::fromUtf8("Umnoj"));
        Umnoj->setGeometry(QRect(1, 103, 178, 18));
        Umnoj->setFont(font);
        Umnoj->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        Det = new QPushButton(centralwidget);
        Det->setObjectName(QString::fromUtf8("Det"));
        Det->setGeometry(QRect(1, 203, 178, 16));
        Det->setFont(font2);
        Det->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        Inverse = new QPushButton(centralwidget);
        Inverse->setObjectName(QString::fromUtf8("Inverse"));
        Inverse->setGeometry(QRect(0, 248, 178, 16));
        Inverse->setFont(font2);
        Inverse->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        VectorProduct = new QPushButton(centralwidget);
        VectorProduct->setObjectName(QString::fromUtf8("VectorProduct"));
        VectorProduct->setGeometry(QRect(0, 272, 178, 18));
        VectorProduct->setFont(font2);
        VectorProduct->setAutoFillBackground(false);
        VectorProduct->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        VectorLength = new QPushButton(centralwidget);
        VectorLength->setObjectName(QString::fromUtf8("VectorLength"));
        VectorLength->setGeometry(QRect(0, 324, 178, 18));
        VectorLength->setFont(font2);
        VectorLength->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        ScalarProduct = new QPushButton(centralwidget);
        ScalarProduct->setObjectName(QString::fromUtf8("ScalarProduct"));
        ScalarProduct->setGeometry(QRect(0, 298, 178, 18));
        ScalarProduct->setFont(font2);
        ScalarProduct->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        X = new QPushButton(centralwidget);
        X->setObjectName(QString::fromUtf8("X"));
        X->setGeometry(QRect(785, 2, 14, 14));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Segoe Print"));
        font3.setPointSize(7);
        font3.setBold(true);
        font3.setWeight(75);
        X->setFont(font3);
        X->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"qproperty-alignment: 'AlignCenter | AlignCenter';\n"
"  color:#8d0000;\n"
" background-color:#ff7777;\n"
"border-radius: 6px;\n"
"}\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(255,119,119), stop: 1 rgb(180,0,0));\n"
"border: 1px solid white;\n"
"} "));
        turn = new QPushButton(centralwidget);
        turn->setObjectName(QString::fromUtf8("turn"));
        turn->setGeometry(QRect(769, 2, 14, 14));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Yu Gothic UI Semibold"));
        font4.setPointSize(14);
        font4.setBold(true);
        font4.setWeight(75);
        turn->setFont(font4);
        turn->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"qproperty-alignment: 'AlignTop | AlignRight';\n"
"  color:#9f9400;\n"
" background-color: #ffff7f;\n"
"border-radius: 6px;\n"
"}\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(255,255,30), stop: 1 rgb(255,179,0));\n"
"border: 1px solid white;\n"
"} "));
        GuidesCos = new QPushButton(centralwidget);
        GuidesCos->setObjectName(QString::fromUtf8("GuidesCos"));
        GuidesCos->setGeometry(QRect(0, 350, 178, 18));
        GuidesCos->setFont(font2);
        GuidesCos->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color: #2550be;\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        strokiA->setText(QCoreApplication::translate("MainWindow", "\320\241\321\202\321\200\320\276\320\272\320\270:", nullptr));
        stolbciA->setText(QCoreApplication::translate("MainWindow", "\320\241\321\202\320\276\320\273\320\261\321\206\321\213:", nullptr));
        strokiB->setText(QCoreApplication::translate("MainWindow", "\320\241\321\202\321\200\320\276\320\272\320\270:", nullptr));
        stolbciB->setText(QCoreApplication::translate("MainWindow", "\320\241\321\202\320\276\320\273\320\261\321\206\321\213:", nullptr));
        Rez->setText(QCoreApplication::translate("MainWindow", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202:", nullptr));
        A->setText(QCoreApplication::translate("MainWindow", "\320\220", nullptr));
        B->setText(QCoreApplication::translate("MainWindow", "\320\222", nullptr));
        liniya1->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        liniya2->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        liniya3->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        liniya4->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        znachA->setTitle(QString());
        znachB->setTitle(QString());
        okA->setText(QCoreApplication::translate("MainWindow", "OK", nullptr));
        okB->setText(QCoreApplication::translate("MainWindow", "OK", nullptr));
        znachC->setTitle(QString());
        stepenline->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        nachisloline->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        Summa->setText(QCoreApplication::translate("MainWindow", " A + B", nullptr));
        Nachislo->setText(QCoreApplication::translate("MainWindow", "Multiply by number A", nullptr));
        Transpose->setText(QCoreApplication::translate("MainWindow", "Transpose A", nullptr));
        Slae->setText(QCoreApplication::translate("MainWindow", "SLAE", nullptr));
        Raznost->setText(QCoreApplication::translate("MainWindow", "A - B", nullptr));
        Stepen->setText(QCoreApplication::translate("MainWindow", "         A^(n)", nullptr));
        Umnoj->setText(QCoreApplication::translate("MainWindow", "A * B", nullptr));
        Det->setText(QCoreApplication::translate("MainWindow", " Determinant A ", nullptr));
        Inverse->setText(QCoreApplication::translate("MainWindow", "Inverse A", nullptr));
        VectorProduct->setText(QCoreApplication::translate("MainWindow", "Vector product", nullptr));
        VectorLength->setText(QCoreApplication::translate("MainWindow", "Vector length A", nullptr));
        ScalarProduct->setText(QCoreApplication::translate("MainWindow", "Scalar product", nullptr));
        X->setText(QCoreApplication::translate("MainWindow", "X", nullptr));
        turn->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        GuidesCos->setText(QCoreApplication::translate("MainWindow", "Guides cos() A", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GENERAL_H
