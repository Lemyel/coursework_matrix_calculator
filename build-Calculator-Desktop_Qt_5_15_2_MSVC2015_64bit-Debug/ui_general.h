/********************************************************************************
** Form generated from reading UI file 'general.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GENERAL_H
#define UI_GENERAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *layoutWidget;
    QGridLayout *foo;
    QPushButton *Summa;
    QPushButton *Nachislo;
    QPushButton *Transpose;
    QPushButton *Slae;
    QPushButton *Raznost;
    QPushButton *Stepen;
    QPushButton *Umnoj;
    QPushButton *Det;
    QLabel *strokiA;
    QLabel *stolbciA;
    QLabel *strokiB;
    QLabel *stolbciB;
    QLabel *Rez;
    QLabel *A;
    QLabel *B;
    QGroupBox *Calc;
    QPushButton *pushButton_6;
    QPushButton *pushButton_dot;
    QPushButton *pushButton_0;
    QPushButton *pushButton_1;
    QPushButton *pushButton_2;
    QPushButton *pushButton_5;
    QPushButton *pushButton_9;
    QPushButton *pushButton_4;
    QPushButton *pushButton_8;
    QPushButton *pushButton_3;
    QPushButton *pushButton_7;
    QLabel *liniya1;
    QLabel *liniya2;
    QLabel *liniya3;
    QLabel *liniya4;
    QGroupBox *znachA;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout;
    QGroupBox *znachB;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout_2;
    QLineEdit *line_strokiA;
    QLineEdit *line_stolbciA;
    QLineEdit *line_strokiB;
    QLineEdit *line_stolbciB;
    QPushButton *pushButton;
    QPushButton *pushButton_10;
    QGroupBox *znachC;
    QWidget *layoutWidget_2;
    QGridLayout *gridLayout_4;
    QLineEdit *labe2;
    QLabel *labe3;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 436);
        MainWindow->setStyleSheet(QString::fromUtf8("MainWindow{\n"
"  background-color: rgb(36,68,162);}"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        layoutWidget = new QWidget(centralwidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 181, 199));
        foo = new QGridLayout(layoutWidget);
        foo->setObjectName(QString::fromUtf8("foo"));
        foo->setContentsMargins(0, 0, 0, 0);
        Summa = new QPushButton(layoutWidget);
        Summa->setObjectName(QString::fromUtf8("Summa"));
        QFont font;
        font.setPointSize(11);
        font.setBold(true);
        font.setWeight(75);
        Summa->setFont(font);
        Summa->setAutoFillBackground(false);
        Summa->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));

        foo->addWidget(Summa, 0, 0, 1, 1);

        Nachislo = new QPushButton(layoutWidget);
        Nachislo->setObjectName(QString::fromUtf8("Nachislo"));
        QFont font1;
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        Nachislo->setFont(font1);
        Nachislo->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));

        foo->addWidget(Nachislo, 4, 0, 1, 1);

        Transpose = new QPushButton(layoutWidget);
        Transpose->setObjectName(QString::fromUtf8("Transpose"));
        Transpose->setFont(font1);
        Transpose->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));

        foo->addWidget(Transpose, 5, 0, 1, 1);

        Slae = new QPushButton(layoutWidget);
        Slae->setObjectName(QString::fromUtf8("Slae"));
        Slae->setFont(font1);
        Slae->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));

        foo->addWidget(Slae, 7, 0, 1, 1);

        Raznost = new QPushButton(layoutWidget);
        Raznost->setObjectName(QString::fromUtf8("Raznost"));
        Raznost->setFont(font);
        Raznost->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));

        foo->addWidget(Raznost, 1, 0, 1, 1);

        Stepen = new QPushButton(layoutWidget);
        Stepen->setObjectName(QString::fromUtf8("Stepen"));
        Stepen->setFont(font);
        Stepen->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));

        foo->addWidget(Stepen, 3, 0, 1, 1);

        Umnoj = new QPushButton(layoutWidget);
        Umnoj->setObjectName(QString::fromUtf8("Umnoj"));
        Umnoj->setFont(font);
        Umnoj->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));

        foo->addWidget(Umnoj, 2, 0, 1, 1);

        Det = new QPushButton(layoutWidget);
        Det->setObjectName(QString::fromUtf8("Det"));
        Det->setFont(font1);
        Det->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"  border-style:solid;\n"
"  border-color: #2442a2;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));

        foo->addWidget(Det, 6, 0, 1, 1);

        strokiA = new QLabel(centralwidget);
        strokiA->setObjectName(QString::fromUtf8("strokiA"));
        strokiA->setGeometry(QRect(240, 30, 64, 22));
        strokiA->setFont(font);
        strokiA->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        stolbciA = new QLabel(centralwidget);
        stolbciA->setObjectName(QString::fromUtf8("stolbciA"));
        stolbciA->setGeometry(QRect(360, 30, 79, 22));
        stolbciA->setFont(font);
        stolbciA->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        strokiB = new QLabel(centralwidget);
        strokiB->setObjectName(QString::fromUtf8("strokiB"));
        strokiB->setGeometry(QRect(510, 30, 64, 22));
        strokiB->setFont(font);
        strokiB->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        stolbciB = new QLabel(centralwidget);
        stolbciB->setObjectName(QString::fromUtf8("stolbciB"));
        stolbciB->setGeometry(QRect(640, 30, 79, 22));
        stolbciB->setFont(font);
        stolbciB->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        Rez = new QLabel(centralwidget);
        Rez->setObjectName(QString::fromUtf8("Rez"));
        Rez->setGeometry(QRect(240, 250, 86, 22));
        Rez->setFont(font);
        Rez->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        A = new QLabel(centralwidget);
        A->setObjectName(QString::fromUtf8("A"));
        A->setGeometry(QRect(310, 0, 41, 20));
        QFont font2;
        font2.setPointSize(14);
        font2.setBold(true);
        font2.setItalic(false);
        font2.setUnderline(true);
        font2.setWeight(75);
        A->setFont(font2);
        A->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}\n"
""));
        B = new QLabel(centralwidget);
        B->setObjectName(QString::fromUtf8("B"));
        B->setGeometry(QRect(590, 0, 41, 20));
        B->setFont(font2);
        B->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:white;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        Calc = new QGroupBox(centralwidget);
        Calc->setObjectName(QString::fromUtf8("Calc"));
        Calc->setGeometry(QRect(0, 200, 189, 243));
        Calc->setStyleSheet(QString::fromUtf8("QGroupBox\n"
"{border: 0px;\n"
"\n"
"}"));
        pushButton_6 = new QPushButton(Calc);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setGeometry(QRect(120, 60, 61, 61));
        QFont font3;
        font3.setPointSize(14);
        font3.setBold(true);
        font3.setWeight(75);
        pushButton_6->setFont(font3);
        pushButton_6->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"border: 2px solid white;\n"
"  border-style:solid;\n"
"  border-color: #ffffff;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        pushButton_dot = new QPushButton(Calc);
        pushButton_dot->setObjectName(QString::fromUtf8("pushButton_dot"));
        pushButton_dot->setGeometry(QRect(120, 180, 61, 61));
        pushButton_dot->setFont(font3);
        pushButton_dot->setLayoutDirection(Qt::LeftToRight);
        pushButton_dot->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"border: 2px solid white;\n"
"  border-style:solid;\n"
"  border-color: #ffffff;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        pushButton_0 = new QPushButton(Calc);
        pushButton_0->setObjectName(QString::fromUtf8("pushButton_0"));
        pushButton_0->setGeometry(QRect(-1, 180, 121, 61));
        pushButton_0->setFont(font3);
        pushButton_0->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"border: 2px solid white;\n"
"  border-style:solid;\n"
"  border-color: #ffffff;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        pushButton_1 = new QPushButton(Calc);
        pushButton_1->setObjectName(QString::fromUtf8("pushButton_1"));
        pushButton_1->setGeometry(QRect(-2, 120, 61, 61));
        pushButton_1->setFont(font3);
        pushButton_1->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"border: 2px solid white;\n"
"  border-style:solid;\n"
"  border-color: #ffffff;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        pushButton_2 = new QPushButton(Calc);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(59, 120, 61, 61));
        pushButton_2->setFont(font3);
        pushButton_2->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"border: 2px solid white;\n"
"  border-style:solid;\n"
"  border-color: #ffffff;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        pushButton_5 = new QPushButton(Calc);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setGeometry(QRect(59, 60, 61, 61));
        pushButton_5->setFont(font3);
        pushButton_5->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"border: 2px solid white;\n"
"  border-style:solid;\n"
"  border-color: #ffffff;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        pushButton_9 = new QPushButton(Calc);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        pushButton_9->setGeometry(QRect(120, -1, 61, 61));
        pushButton_9->setFont(font3);
        pushButton_9->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"border: 2px solid white;\n"
"  border-style:solid;\n"
"  border-color: #ffffff;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        pushButton_4 = new QPushButton(Calc);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setGeometry(QRect(-2, 60, 61, 61));
        pushButton_4->setFont(font3);
        pushButton_4->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"border: 2px solid white;\n"
"  border-style:solid;\n"
"  border-color: #ffffff;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        pushButton_8 = new QPushButton(Calc);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setGeometry(QRect(59, -1, 61, 61));
        pushButton_8->setFont(font3);
        pushButton_8->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"border: 2px solid white;\n"
"  border-style:solid;\n"
"  border-color: #ffffff;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        pushButton_3 = new QPushButton(Calc);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(120, 120, 61, 61));
        pushButton_3->setFont(font3);
        pushButton_3->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"border: 2px solid white;\n"
"  border-style:solid;\n"
"  border-color: #ffffff;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        pushButton_7 = new QPushButton(Calc);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setGeometry(QRect(-2, -1, 61, 61));
        pushButton_7->setFont(font3);
        pushButton_7->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"  background-color:rgb(2, 23, 109);\n"
"  color: white; \n"
"border: 2px solid white;\n"
"  border-style:solid;\n"
"  border-color: #ffffff;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(133,149,255), stop: 1 rgb(54,101,243));\n"
"} "));
        liniya1 = new QLabel(centralwidget);
        liniya1->setObjectName(QString::fromUtf8("liniya1"));
        liniya1->setGeometry(QRect(180, 60, 623, 2));
        liniya1->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:#2442a2;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        liniya2 = new QLabel(centralwidget);
        liniya2->setObjectName(QString::fromUtf8("liniya2"));
        liniya2->setGeometry(QRect(180, 250, 622, 2));
        liniya2->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:#2442a2;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        liniya3 = new QLabel(centralwidget);
        liniya3->setObjectName(QString::fromUtf8("liniya3"));
        liniya3->setGeometry(QRect(490, 60, 1, 192));
        liniya3->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:#2442a2;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        liniya4 = new QLabel(centralwidget);
        liniya4->setObjectName(QString::fromUtf8("liniya4"));
        liniya4->setGeometry(QRect(179, -9, 2, 214));
        liniya4->setStyleSheet(QString::fromUtf8("QLabel {\n"
"qproperty-alignment: 'AlignVCenter | AlignCenter';\n"
"  border: 1px solid white;\n"
"  color:#2442a2;\n"
"  border-style:solid;\n"
" background-color: #2442a2;\n"
"}"));
        znachA = new QGroupBox(centralwidget);
        znachA->setObjectName(QString::fromUtf8("znachA"));
        znachA->setGeometry(QRect(199, 62, 281, 188));
        znachA->setStyleSheet(QString::fromUtf8("QGroupBox{\n"
"  border: 0px;\n"
"  color:white;\n"
" background-color: #2442a2;\n"
"}"));
        layoutWidget1 = new QWidget(znachA);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(0, 0, 281, 191));
        gridLayout = new QGridLayout(layoutWidget1);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        znachB = new QGroupBox(centralwidget);
        znachB->setObjectName(QString::fromUtf8("znachB"));
        znachB->setGeometry(QRect(500, 62, 281, 188));
        znachB->setStyleSheet(QString::fromUtf8("QGroupBox{\n"
"  border: 0px;\n"
"  color:white;\n"
" background-color: #2442a2;\n"
"}"));
        gridLayoutWidget = new QWidget(znachB);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(-1, -1, 281, 191));
        gridLayout_2 = new QGridLayout(gridLayoutWidget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        line_strokiA = new QLineEdit(centralwidget);
        line_strokiA->setObjectName(QString::fromUtf8("line_strokiA"));
        line_strokiA->setGeometry(QRect(305, 30, 31, 22));
        line_stolbciA = new QLineEdit(centralwidget);
        line_stolbciA->setObjectName(QString::fromUtf8("line_stolbciA"));
        line_stolbciA->setGeometry(QRect(440, 30, 31, 22));
        line_strokiB = new QLineEdit(centralwidget);
        line_strokiB->setObjectName(QString::fromUtf8("line_strokiB"));
        line_strokiB->setGeometry(QRect(575, 30, 31, 22));
        line_stolbciB = new QLineEdit(centralwidget);
        line_stolbciB->setObjectName(QString::fromUtf8("line_stolbciB"));
        line_stolbciB->setGeometry(QRect(720, 30, 31, 22));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(360, 0, 75, 23));
        pushButton_10 = new QPushButton(centralwidget);
        pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));
        pushButton_10->setGeometry(QRect(640, 0, 75, 23));
        znachC = new QGroupBox(centralwidget);
        znachC->setObjectName(QString::fromUtf8("znachC"));
        znachC->setGeometry(QRect(340, 253, 281, 183));
        znachC->setStyleSheet(QString::fromUtf8("QGroupBox{\n"
"  border: 0px;\n"
"  color:white;\n"
" background-color: #2442a2;\n"
"}"));
        layoutWidget_2 = new QWidget(znachC);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(0, 0, 281, 181));
        gridLayout_4 = new QGridLayout(layoutWidget_2);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        labe2 = new QLineEdit(centralwidget);
        labe2->setObjectName(QString::fromUtf8("labe2"));
        labe2->setGeometry(QRect(660, 280, 113, 20));
        labe3 = new QLabel(centralwidget);
        labe3->setObjectName(QString::fromUtf8("labe3"));
        labe3->setGeometry(QRect(670, 280, 91, 41));
        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        Summa->setText(QCoreApplication::translate("MainWindow", "A + B", nullptr));
        Nachislo->setText(QCoreApplication::translate("MainWindow", "Multiply by number", nullptr));
        Transpose->setText(QCoreApplication::translate("MainWindow", "Transpose", nullptr));
        Slae->setText(QCoreApplication::translate("MainWindow", "SLAE", nullptr));
        Raznost->setText(QCoreApplication::translate("MainWindow", "A - B", nullptr));
        Stepen->setText(QCoreApplication::translate("MainWindow", "A^(n)", nullptr));
        Umnoj->setText(QCoreApplication::translate("MainWindow", "A * B", nullptr));
        Det->setText(QCoreApplication::translate("MainWindow", "Determinant", nullptr));
        strokiA->setText(QCoreApplication::translate("MainWindow", "\320\241\321\202\321\200\320\276\320\272\320\270:", nullptr));
        stolbciA->setText(QCoreApplication::translate("MainWindow", "\320\241\321\202\320\276\320\273\320\261\321\206\321\213:", nullptr));
        strokiB->setText(QCoreApplication::translate("MainWindow", "\320\241\321\202\321\200\320\276\320\272\320\270:", nullptr));
        stolbciB->setText(QCoreApplication::translate("MainWindow", "\320\241\321\202\320\276\320\273\320\261\321\206\321\213:", nullptr));
        Rez->setText(QCoreApplication::translate("MainWindow", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202:", nullptr));
        A->setText(QCoreApplication::translate("MainWindow", "\320\220", nullptr));
        B->setText(QCoreApplication::translate("MainWindow", "\320\222", nullptr));
        Calc->setTitle(QCoreApplication::translate("MainWindow", "GroupBox", nullptr));
        pushButton_6->setText(QCoreApplication::translate("MainWindow", "6", nullptr));
        pushButton_dot->setText(QCoreApplication::translate("MainWindow", ".", nullptr));
        pushButton_0->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        pushButton_1->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        pushButton_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        pushButton_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        pushButton_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        pushButton_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        pushButton_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        pushButton_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        pushButton_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        liniya1->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        liniya2->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        liniya3->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        liniya4->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        znachA->setTitle(QString());
        znachB->setTitle(QString());
        pushButton->setText(QCoreApplication::translate("MainWindow", "OK", nullptr));
        pushButton_10->setText(QCoreApplication::translate("MainWindow", "OK", nullptr));
        znachC->setTitle(QString());
        labe3->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GENERAL_H
